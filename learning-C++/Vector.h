#pragma once
#include <iostream>
#include <cmath>
class Vector
{
private:
    int m_beginX, m_beginY, m_beginZ;
    int m_endX,   m_endY,   m_endZ;

public:
    Vector();
    Vector(int endX, int endY, int endZ);
    Vector(int beginX, int beginY, int beginZ, int endX, int endY, int endZ);

    int getVectorLenght() const;

    friend Vector operator+ (const Vector& vec1, const Vector& vec2);
    friend std::ostream& operator<< (std::ostream& out, const Vector& vec);
};

Vector::Vector() :
    m_beginX(0), m_beginY(0), m_beginZ(0),
    m_endX(0), m_endY(0), m_endZ(0)
{
}

inline Vector::Vector(int endX, int endY, int endZ) :
    m_beginX(0), m_beginY(0), m_beginZ(0),
    m_endX(endX), m_endY(endY), m_endZ(endZ)
{
}

inline Vector::Vector(int beginX, int beginY, int beginZ, int endX, int endY, int endZ) :
    m_beginX(beginX), m_beginY(beginY), m_beginZ(beginZ),
    m_endX(endX), m_endY(endY), m_endZ(endZ)
{
}

inline int Vector::getVectorLenght() const
{
    double sumOfCoordsSquares = std::pow((m_beginX + m_endX), 2.) + std::pow((m_beginY + m_endY), 2.) + std::pow((m_beginZ + m_endZ), 2.);
    return static_cast<int>(std::sqrt(sumOfCoordsSquares));
}

Vector operator+ (const Vector& vec1, const Vector& vec2)
{
    return Vector(                      //
        vec1.m_beginX + vec2.m_beginX,  //
        vec1.m_beginY + vec2.m_beginY,  //
        vec1.m_beginZ + vec2.m_beginZ,  //
        vec1.m_endX + vec2.m_endX,      //
        vec1.m_endY + vec2.m_endY,      //
        vec1.m_endZ + vec2.m_endZ);     //
}

std::ostream& operator<< (std::ostream& out, const Vector& vec)
{
     out << "( " << vec.m_beginX << ", " << vec.m_beginY << ", " << vec.m_beginZ << " )" <<
        "( " << vec.m_endX << ", " << vec.m_endY << ", " << vec.m_endZ << " )\n";

    return out;
}   