#pragma once
#include <iostream>

class Animal
{
public:
    virtual void Voice() = 0;
};
void Animal::Voice() { std::cout << "Pfffff "; }

class Cat : public Animal
{
    virtual void Voice() override { std::cout << "Meow\n"; }
};

class Dog : public Animal
{
    virtual void Voice() override { std::cout << "Woof\n"; }
};

class Pig : public Animal
{
    virtual void Voice() override { std::cout << "Oink\n"; }
};

class Mouse : public Animal
{
    virtual void Voice() override { std::cout << "Squeak\n"; }
};