#pragma once

int SumToSquare(int a, int b)
{
    return (a + b) * (a + b);
}