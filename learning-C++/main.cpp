#include <iostream>
#include <string>
#include <iomanip>
#include <time.h>
#include <vector>
#include "Helpers.h"
#include "Vector.h"
#include "LiteStack.h"
#include "Animal.h"

void PrintOddNumbers(int Number, bool isOdd)
{
    if (isOdd)
    {
        std::cout << Number << "\n";
    }
}

struct Favorite
{
    int a;
    double b;
    Favorite(): a(0), b(0) {}
    Favorite(int x, double y): a(x), b(y) {}

    friend std::ostream& operator<<(std::ostream& out, const Favorite& obj)
    {
        out << obj.a << ' ' << obj.b << ' ';
        return out;
    }
};

int main()
{
    //std::cout << SumToSquare(7, 8);

    /*
    std::string myString;
    std::cout << "Enter string: ";
    std::getline(std::cin, myString);

    if (myString.size() == 0) 
    {   
        std::cout << "No any symbols";
        return 0;
    }

    std::cout << "You have typed: " << myString << " length: " << myString.size()
        << "\n first character: " << myString.front() << "\n last character: " << myString.back();
    */
	/*
    // 1st case
    const int FinalNumber = 20;

    for (int i = 0; i <= FinalNumber; ++i)
    {
        PrintOddNumbers(i, i % 2 == 0);
    }

    // 2nd case
    for (int i = 0; i <= FinalNumber; i += 2)
    {
        std::cout << i << "\n";
    }
	*/
    /*
    const int N = 7;

    int myArray[N][N];
    for (int i = 0; i < N; ++i)
    {
        for (int j = 0; j < N; ++j)
        {
            myArray[i][j] = i + j;
            std::cout << std::setw(2) << myArray[i][j] << ' ';
        }
        std::cout << '\n';
    }

    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    
    int rawFoundByDate = buf.tm_mday % N;
    int rawSum = 0;

    for (int i = 0; i < N; ++i)
    {
        rawSum += myArray[rawFoundByDate][i];
    }

    std::cout << rawSum;
    */
    /*
    Vector vec1(2,1,3,4,1,5);
    Vector vec2(2,7,3);
    Vector vec3 = vec1 + vec2;

    std::cout << "vec1: " << vec1 << 
        "vec2: " << vec2 <<
        "vec1 + vec2 = vec3: " << vec3 <<
        "lenght vec3: " << vec3.getVectorLenght();
    */
    /*
    LiteStack<int> intStack;

    intStack.push(7);
    intStack.push(2);
    intStack.push(17);
    intStack.push(10);
    intStack.push(5);
    intStack.push(15);
    intStack.print();

    intStack.pop();
    intStack.pop();
    intStack.print();

    intStack.push(10);
    intStack.push(20);
    intStack.empty();
    intStack.print();

    intStack.push(15);
    intStack.print();

    LiteStack<std::string> stringStack;
    stringStack.push("first string");
    std::string Second = "second string";
    stringStack.push(Second);
    stringStack.print();

    LiteStack<Favorite> structStack;
    structStack.push(Favorite(7, 8.5));
    structStack.print();
    */
    std::vector<Animal*> zoo;
    zoo.push_back(new Cat());
    zoo.push_back(new Dog());
    zoo.push_back(new Pig());
    zoo.push_back(new Mouse());

    for (const auto& animal : zoo)
    {
        animal->Voice();
    }

    return 0;
}