#pragma once

template<typename T>
class LiteStack
{
private:
    T* elements;
    const int defaultSize = 5;
    int stackSize;
    int emptyElementIndex = 0;

public:
    LiteStack();
    ~LiteStack();

    void pop();
    void push(T element);
    void print();
    T get() const;
    void empty();

private:
    void setDefaultElements();
    void resizeStack(int size);
};

template<typename T>
LiteStack<T>::LiteStack()
{
    stackSize = defaultSize;
    elements = new T[stackSize];
    setDefaultElements();
}

template<typename T>
LiteStack<T>::~LiteStack()
{
    delete[] elements;
}

template<typename T>
void LiteStack<T>::push(T element)
{
    std::cout << "elements: " << emptyElementIndex << ' ' << "size: " << stackSize << '\n';
    
    if (emptyElementIndex == stackSize)
    {
        resizeStack(stackSize * 2);
    }

    elements[emptyElementIndex] = element;
    std::cout << "element added\n";
    ++emptyElementIndex;
    std::cout << "elements: " << emptyElementIndex << ' ' << "size: " << stackSize << '\n';
}

template<typename T>
void LiteStack<T>::pop()
{
    --emptyElementIndex;
    if (emptyElementIndex < 0) return;

    if (emptyElementIndex < stackSize / 4 && stackSize / 2 >= defaultSize)
    {
        resizeStack(stackSize / 2);
    }
    
    elements[emptyElementIndex] = 0;
    
    std::cout << "elements: " << emptyElementIndex << ' ' << "size: " << stackSize << '\n';
}

template<typename T>
void LiteStack<T>::print()
{
    for (T* ptr = &elements[0]; ptr < elements + stackSize; ++ptr)
    {
        std::cout << *ptr << ' ';
    }
    std::cout << '\n';
}

template<typename T>
inline T LiteStack<T>::get() const
{
    if (emptyElementIndex <= 0) return 0;
    return elements[emptyElementIndex - 1];
}

template<typename T>
void LiteStack<T>::empty()
{
    emptyElementIndex = 0;
    //setDefaultElements();
    resizeStack(defaultSize);
}

template<typename T>
void LiteStack<T>::setDefaultElements()
{
    for (int i = 0; i < stackSize; ++i)
    {
        elements[i] = T();
    }
}

template<typename T>
inline void LiteStack<T>::resizeStack(int size)
{
    T* tempStack = new T[stackSize];
    for (int i = 0; i < stackSize; ++i)
    {
        tempStack[i] = elements[i];
    }
    delete[] elements;
    elements = nullptr;
    
    stackSize = size;

    elements = new T[stackSize];
    setDefaultElements();

    for (int i = 0; i < emptyElementIndex; ++i)
    {
        elements[i] = tempStack[i];
    }
    delete[] tempStack;
    tempStack = nullptr;
    
    std::cout << "stack resized\n";
}
